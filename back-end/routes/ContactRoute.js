const express=require('express');

const router=express.Router();
const { body } = require("express-validator");
const ContactController=require('../controllers/contactController') 


let validators = [
    body("nom").notEmpty().trim().escape().withMessage("Vous devez renseigner le nom"),
    body("prenom").not().isEmpty().withMessage("Vous devez renseigner le prénom"),
    body("email")
    .notEmpty().withMessage("L'email est obligatoire")
    .isEmail().withMessage("Le format email est obligatoire"),
    body("telephone")
      .notEmpty().trim().escape().withMessage('le numéro de téléphone svp')
      .isLength({ min: 8, max: 8 }).withMessage("Le numéro de téléphone doit être de 8 chiffres "),
  ];
  

router.get('/',ContactController.listContact)
router.post('/add',validators,ContactController.createContact)
router.post('/delete',ContactController.deleteContact)
module.exports=router