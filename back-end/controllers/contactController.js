const Contact = require("../models/Contact");
const mongoose = require("mongoose");
const {  validationResult } = require("express-validator");

const listContact = async (_,res) => {
  try {
    const contacts = await Contact.find({}).exec();

    return res
      .status(200)
      .json({ data: contacts, message: "contact list", success: true });
  } catch (error) {
    return res.status(500).json({ message: error, success: false });
  }
};

const createContact = async (req, res) => {
  
  try {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array(),success:false });
  }

    let item = {
      nom: req.body.nom,
      prenom: req.body.prenom,
      email: req.body.email,
      telephone: req.body.telephone,
    };
    const contactItem = await Contact.create(item);
    return res.status(200).json({
      data: contactItem,
      message: "contact created successfully",
      success: true,
    });
  } catch (error) {
    if (error instanceof mongoose.Error.ValidationError) {
      return res
        .status(400)
        .json({ message: error.message, data: error.errors });
    }
   else if (error.code==11000) {
      return res
        .status(401)
        .json({ message: "l'email doit etre unique",success:false });
    } 
    return res
      .status(500)
      .json({ error, message: "An error occured", success: false });
  }

  
};
const deleteContact=async (req,res)=>{
  try{
  
const contactDelete=await Contact.deleteMany({_id:{$in:req.body}});
return res.status(200).json({
  data: contactDelete,
  message: "all items deleted successfully",
  success: true,
});
  }catch(error){
    return res
    .status(500)
    .json({ error, message: "An error occured", success: false });
  }
}
module.exports = { listContact, createContact ,deleteContact};
