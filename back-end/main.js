const express = require("express");
const morgan = require("morgan");
const bodyParser = require("body-parser");
var cors = require("cors");
const mongoose = require("mongoose");
const dotenv = require("dotenv");
const ContactRoute = require("./routes/ContactRoute");
dotenv.config();

mongoose.connect(
  `mongodb://${process.env.MONGO_URL}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}`
);
const db = mongoose.connection;

db.on("error", (err) => {
  console.log(err);
});

db.once("open", () => {
  console.log("DB connection established");
});

const app = express();

app.use(morgan("dev")).use(bodyParser.json()).use(cors());

app.use("/api/contact", ContactRoute);
app.use(({ res }) => {
  const message =
    "Impossible de trouver la ressource demandée ! Vous pouvez essayer une autre URL.";
  res.status(404).json({ message });
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running on the port ${PORT} `);
});
