const mongoose=require('mongoose')

const Schema=mongoose.Schema
const contactSchema=new Schema({
    nom:{
        type:String,
        required:[true,'Veuillez entrer le nom']
    },
   prenom:{
        type:String,
        required:[true,'Veuillez entrer le prenom']
    },
    email:{
        type:String,
        required:[true,"L'email est obligatoire"],
        unique:[true,"ce email existe deja"],
        
    },
    telephone:{
        type:String,        
        minlength:[8,'Veuillez entrer un numero a 8 chiffres'],
        maxlength:[8,'Veuillez entrer un numero a 8 chiffres']
    },
   
},{timestamp:true})

const Contact=mongoose.model('Contact',contactSchema)

module.exports = Contact